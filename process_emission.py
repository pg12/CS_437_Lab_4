import json
import logging
import sys

import greengrasssdk

# Logging
logger = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
# print("Starting log")

# SDK Client
client = greengrasssdk.client("iot-data")

# Counter
max_value = 0
def lambda_handler(event, context):
    # print("Handler got called!")
    global my_counter
    #TODO1: Get your data
    co2 = event['co2']
    timeStamp = event['time_stamp']
    vehicle = event['vehicle']

    vehicle_num = vehicle.split(".")[0]
    vehicle_num = vehicle[len(vehicle_num) - 1]

    #TODO2: Calculate max CO2 emission
    max_value = max(co2, max_value) 

    #TODO3: Return the result
    client.publish(
        topic= "iot/Vehicle_" + vehicle_num,
        queueFullPolicy="AllOrException",
        payload = json.dumps({"co2": co2, "max_co2": max_value, "time_stamp": timeStamp, "vehicle": vehicle }),
    )
    
    print("Publishing to: iot/Vehicle_" + vehicle_num)

    return